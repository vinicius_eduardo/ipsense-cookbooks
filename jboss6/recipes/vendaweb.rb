#
# Cookbook Name:: jboss6
# Recipe:: vendaweb
#
#
node['jboss6']['vendaweb']['context'].each do |app,config|
  create_context "create context - #{app}" do
    app_name app
  end
  
  install_libs "install libs - #{app}" do
    app_name app
    dl_libs node['jboss6']['vendaweb']['lib']
  end
  
  create_datasource_vendaweb "create datasource - #{app}" do
    app_name app
    db_name config['db_name']
  end

  configure_bindings "configure bindings - #{app}" do
    app_name app
    app_port_binding config["port_binding"]
    app_port_offset config["port_offset"]
  end

  service_app "create service - #{app}" do
    app_name app
  end
end