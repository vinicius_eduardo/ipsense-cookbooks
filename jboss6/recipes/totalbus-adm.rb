#
# Cookbook Name:: jboss6
# Recipe:: totalbus-adm
#
#
node['jboss6']['apps']['totalbus-adm'].each do |app,config|  
  create_context "create context - #{app}" do
    app_name app
  end

  create_datasource "create datasource - #{app}" do
    app_name app
    read_ds true
  end
  
  configure_bindings "configure bindings - #{app}" do
    app_name app
    app_port_binding config["port_binding"]
    app_port_offset config["port_offset"]
  end

  service_app "create service - #{app}" do
    app_name app
  end
end