define :service_app  do  
  app = params[:app_name]
  app_port_offset = params[:app_port_offset]
  jboss_shutdown_port = 1090 + Integer(app_port_offset)

  template "/etc/init.d/jboss_#{app}" do
    source "jboss-init-debian.erb"
    mode 0775
    owner "root"
    group "root"
    variables({
        :jboss_home => "#{node['jboss6']['jboss_home']}#{node['jboss6']['jboss_user']}",
        :jboss_user => node['jboss6']['jboss_user'],
        :jboss_bind_ip => node['jboss6']['public_bind_addr'],
        :jboss_conf => "#{app}",
        :jboss_shutdown_port => "#{jboss_shutdown_port}"
    })
    not_if { File.exist?("/etc/init.d/jboss_#{app}") } 
  end
end
