define :create_datasource_vendaweb  do  
  app = params[:app_name]
  app_dir = "#{node['jboss6']['jboss_home']}#{node['jboss6']['jboss_user']}/server/#{app}"
  
  template "#{app_dir}/deploy/vendaweb-ds.xml" do
    source 'datasource/vendaweb-ds.xml.erb'
    mode 0775
    owner "jboss"
    group "jboss"
    variables({
        :ds_connection_url => "jdbc:mysql://mysqldb01.cg09ytaizn5g.us-east-1.rds.amazonaws.com/#{params[:db_name]}"
      })
  end
end