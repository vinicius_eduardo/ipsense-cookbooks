default['jboss6']['jboss_home'] = "/opt/"
default['jboss6']['jboss_user'] = "jboss"
default['jboss6']['jboss_user_dir'] = "/home/"
default['jboss6']['dl_url'] = "https://s3.amazonaws.com/ipsense-devops/packages/jboss/jboss-custom-rj.tar.gz"
default['jboss6']['conf_default'] = "default"
default['jboss6']['jvm_min_mem'] = "512m"
default['jboss6']['jvm_max_mem'] = "1024m"
default['jboss6']['jvm_perm_mem'] = "256m"
default['jboss6']['jvm_extra_ops'] = ""
default['jboss6']['public_bind_addr'] = "0.0.0.0"
default['jboss6']['unsecure_bind_addr'] = "0.0.0.0"
default['jboss6']['admin_user'] = "admin"
default['jboss6']['admin_pass'] = "rjconsultores@2016"
default['jboss6']['install_service'] = false

default['jboss6']['vendaweb']['lib'] = 'http://repositorio-vendaweb.s3-website-us-east-1.amazonaws.com/dependencias/vendaweb-libs.rar'
