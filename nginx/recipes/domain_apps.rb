#
# Cookbook Name:: nginx
# Recipe:: domain_apps
#
node[:deploy].each do |application, deploy|
  if deploy.has_key?(:domains)
    if !deploy[:domains].empty?
      enable_ssl = false
      if deploy[:ssl_support]
        enable_ssl = true

        directory "/etc/ssl/#{application}" do
          owner 'root'
          group 'root'
          mode "0755"
          recursive true
        end

        file "/etc/ssl/#{application}/certificate.crt" do
          content deploy[:ssl_certificate]
          owner 'root'
          group 'root'
          mode '0755'
        end

        file "/etc/ssl/#{application}/certificate.key" do
          content deploy[:ssl_certificate_key]
          owner 'root'
          group 'root'
          mode '0755'
        end

        file "/etc/ssl/#{application}/chain-ca.key" do
          content deploy[:ssl_certificate_ca]
          owner 'root'
          group 'root'
          mode '0755'
        end
      end

      template "#{node['nginx']['dir']}/sites-available/#{application}.conf" do
        source 'domain.conf.erb'
        owner  'root'
        group  node['root_group']
        mode   '0644'
        variables({
          :enable_ssl => enable_ssl,
          :domain_name => deploy[:domains].join(" "),
          :app_name => application,
          :app_dir => "#{deploy[:deploy_to]}/current/#{deploy[:document_root]}"
        })
      end    
      nginx_site "#{application}.conf"
    end
  end
end

service 'nginx' do
  supports :status => true, :restart => true, :reload => true, :stop => true
  action   :restart
end