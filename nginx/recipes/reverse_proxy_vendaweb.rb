#
# Cookbook Name:: nginx
# Recipe:: reverse_proxy_vendaweb
#
node['nginx']['domains'].each do |context, config|
  if config['domain'].nil?
    if node['deploy'][context].empty?
      domain = node['hostname']
    else
      domain = node['deploy'][context]['domains'].join(" ")
    end
  else
    domain = config['domain']
  end
  
  enable_ssl = false
  if !node['deploy'][context].empty? && node['deploy'][context]["ssl_support"]
    enable_ssl = true

    directory "/etc/ssl/#{domain}" do
      owner 'root'
      group 'root'
      mode "0755"
      recursive true
    end

    file "/etc/ssl/#{domain}/certificate.crt" do
      content node['deploy'][context]["ssl_certificate"]
      owner 'root'
      group 'root'
      mode '0755'
    end

    file "/etc/ssl/#{domain}/certificate.key" do
      content node['deploy'][context]["ssl_certificate_key"]
      owner 'root'
      group 'root'
      mode '0755'
    end

    file "/etc/ssl/#{domain}/chain-ca.key" do
      content node['deploy'][context]["ssl_certificate_ca"]
      owner 'root'
      group 'root'
      mode '0755'
    end  
  end
  
  params = {}
  config['apps'].each do |app|
    config_app = node['jboss6']['vendaweb']['context'][app]
    params["#{app}"] = {
      :port => (8080+config_app["port_offset"].to_i),
      :context => config_app['app_context']
    }
  end

  template "#{node['nginx']['dir']}/sites-available/#{domain}.conf" do
    source 'vendaweb-proxy.conf.erb'
    owner  'root'
    group  node['root_group']
    mode   '0644'
    variables({
      :domain_name => domain,
      :enable_ssl => enable_ssl,
      :default_context => config['default_context'], 
      :mobile_context => config['mobile_context'],
      :apps => params}
    )
  end    
  nginx_site "#{domain}.conf"
end

service 'nginx' do
  supports :status => true, :restart => true, :reload => true, :stop => true
  action   :restart
end