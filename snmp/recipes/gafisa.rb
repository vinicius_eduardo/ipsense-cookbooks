#
# Cookbook Name:: snmp
# Recipe:: gafisa
#
# Copyright 2016, Ipsense
#
# All rights reserved - Do Not Redistribute
#
cookbook_file '/etc/snmp/snmpd.conf' do
  source 'snmpd.conf'
  owner 'root'
  group 'root'
  mode '0600'
  notifies :restart, 'service[snmpd]'
end

cookbook_file '/etc/default/snmpd' do
	source 'snmpd'
	owner 'root'
	group 'root'
	mode '0644'
	notifies :restart, 'service[snmpd]'
end

service 'snmpd' do
  supports :status => true, :restart => true, :reload => true
  action [:nothing]
end



