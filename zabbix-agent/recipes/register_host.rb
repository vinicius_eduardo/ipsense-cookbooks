chef_gem 'zabbixapi' do # ~FC009
    compile_time true if respond_to?(:compile_time)
    version node['zabbix']['api-version']
end

require 'zabbixapi'

unless node['zabbix']['url_api'].nil?
    begin
        @@zbx = ZabbixApi.connect(
            :url => node['zabbix']['url_api'],
            :user => node['zabbix']['user_api'],
            :password => node['zabbix']['pass_api']
        )
    rescue Exception => e # rubocop: disable RescueException
        Chef::Log.warn "Couldn't connect to zabbix server, all zabbix provider are non-working." + e.message
    end

    host_group_id = @@zbx.hostgroups.get_or_create(:name => node['zabbix']['host_group'])
    Chef::Log.info("HostGroup: #{host_group_id} - #{node['zabbix']['host_group']}")
    template_id = @@zbx.templates.get_or_create(:host => node['zabbix']['template'])
    Chef::Log.info("Template: #{template_id} - #{node['zabbix']['template']}")    

    host = @@zbx.hosts.create_or_update(
        :host => node['hostname'],
        :name => "(#{node['zabbix']['host_group']}) #{node['hostname']}",
        :interfaces => [
            {
                :type => 1,
                :main => 1,
                :ip => '0.0.0.0',
                :dns => '',
                :port => 10050,
                :useip => 1
            }
        ],
        :groups => [ :groupid => host_group_id ],
        :templates => [ :templateid => template_id ]
    )
    # @@zbx.templates.mass_add(
    #     hosts_id: [host],
    #     templates_id: [template_id]
    # )        
    Chef::Log.info("Registered Host: #{host}")
end